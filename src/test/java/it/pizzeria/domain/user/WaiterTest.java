package it.pizzeria.domain.user;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import it.pizzeria.domain.catalog.Menu;
import it.pizzeria.domain.customer.Customer;
import it.pizzeria.domain.customer.ReflectionTime;
import it.pizzeria.domain.furniture.Table;
import it.pizzeria.domain.order.Order;
import it.pizzeria.domain.waiter.Sofia;
import it.pizzeria.domain.waiter.Waiter;

class WaiterTest {

	private Customer customer;
	private Table table;
	private Waiter waiter;
	
	@BeforeEach
	void init() {
		 customer = Mockito.mock(Customer.class);
		 table = Mockito.mock(Table.class);
		 waiter = new Sofia();
		
	}

	@Test
	void testWaiterInstallCustomerToATable() {
		
		Mockito.when(table.isAvailable()).thenReturn(true);
		
		assertEquals(true, waiter.installCustomerToTable(customer, table));

	}
	
	@Test
	void testWaiterCannotInstallCustomerToATable() {
		
		Mockito.when(table.isAvailable()).thenReturn(false);
		
		assertEquals(false, waiter.installCustomerToTable(customer, table));		
		
	}
	
	@Test
	void testWaiterBringMenuToCustomer() {
		
		Menu menu = Mockito.mock(Menu.class);
		assertEquals(true, waiter.bringMenuToClient(menu, customer));
	}
	
	@Test
	void testWaiterGivesSomeReflectionTimeToCustomer() {
		
		ReflectionTime moment = Mockito.mock(ReflectionTime.class);
		
		assertEquals(true, waiter.givesSomeReflectionTimeToCustomer(moment, customer));
	}	
	
	
	@Test
	void testWaiterTakeOrderFromCustomer() {
				
		assertThat(waiter.takeOrderFromCustomer(customer)).isInstanceOf(Order.class);
		
	}
	

}
