package it.pizzeria.domain.user;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import it.pizzeria.domain.catalog.Menu;
import it.pizzeria.domain.customer.Customer;
import it.pizzeria.domain.customer.Jean;

class CustomerTest {

	@Test
	void testCustomerChooseThreeProductsFromMenu() {
		
		final int NB_DISHES_EXPECTED = 3;
				
		Menu menu = Mockito.mock(Menu.class);
		
		Customer jean = new Jean();
		
		assertEquals(NB_DISHES_EXPECTED, jean.chooseProductsFromMenu(menu).size());
	}

}
