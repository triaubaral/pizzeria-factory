package it.pizzeria.service;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mockito;

import it.pizzeria.domain.catalog.Coca;
import it.pizzeria.domain.catalog.Menu;
import it.pizzeria.domain.catalog.Pizza4Fromages;
import it.pizzeria.domain.catalog.Product;
import it.pizzeria.domain.catalog.Tiramitsu;
import it.pizzeria.domain.customer.Customer;
import it.pizzeria.domain.order.Order;
import it.pizzeria.domain.waiter.Waiter;

class OrderServiceTest {
	
	/**
	 * 
	 * Les sc�narios � tester
	 * 
	 * 1. Le service "Order" permet � Jean de commander une pizza 4 fromages aupr�s de Sofia
	 * 2. Le service "Order" permet � Jean de commander un coca aupr�s de Sofia
	 * 3. Le service "Order" permet � Jean de commander un tiramitsu aupr�s de Sofia
	 * 4. Le service "Order" permet � Jean de commander un menu composer d'une pizza 4 fromages d' un coca et un tiramitsu aupr�s de Sofia
	 * 
	 */
	
		
	private OrderService orderService;			
	private Customer jean;		
	private Waiter sofia;
	
	
	@BeforeEach
	void init() {
		
		orderService = new OrderService();		
		jean = Mockito.mock(Customer.class);		
		sofia = Mockito.mock(Waiter.class);
		
	}
	
	@ParameterizedTest
	@ValueSource(classes = {Pizza4Fromages.class, Coca.class, Tiramitsu.class})
	void testOrderProductReturnAnOrder(Class<?> nomProduit) throws InstantiationException, IllegalAccessException {
		
		Product product = (Product) nomProduit.newInstance();
				
		assertThat(orderService.orderAlaCarte(product, jean, sofia)).isInstanceOf(Order.class);
		
	}
	
	
	@Test
	void testOrderProductsCallOnlyOnceProductsOrdered() {
		
		Menu expectedMenu = Mockito.mock(Menu.class);		
		
		orderService.orderMenu(expectedMenu, jean, sofia);
		
		Mockito.verify(expectedMenu, Mockito.times(1)).getProducts();
		
	}
	
}
