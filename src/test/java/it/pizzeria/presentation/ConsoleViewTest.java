package it.pizzeria.presentation;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import it.pizzeria.domain.order.JeanOrder;

class ConsoleViewTest {
	
	/*
	 * 1. La ConsoleView affiche la commande de Jean de la mani�re suivante : Jean a command� aupr�s de Sofia une pizza4Fromages
	 * 
	 */

	@Test
	void testDisplayJeanOrder() {
		
		ConsoleView consoleView = new ConsoleView();
		
		assertEquals("Jean a command� aupr�s de Sofia une pizza 4 fromages.", consoleView.display(new JeanOrder()));
	}

}
