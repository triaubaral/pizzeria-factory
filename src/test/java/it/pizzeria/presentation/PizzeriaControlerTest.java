package it.pizzeria.presentation;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import it.pizzeria.domain.order.JeanOrder;
import it.pizzeria.domain.order.Order;
import it.pizzeria.service.OrderService;

class PizzeriaControlerTest {

	/*
	 * 1. PizzeriaController traite la commande de Jean 
	 * 2. PizzeriaController transmet la commande � une vue qui affichera son contenu
	 * 
	 */
	
	@Test
	void testProcessJeanOrder() {
		
		PizzeriaController pizzeriaController = new PizzeriaController();
		
		JeanOrder jeanOrder = new JeanOrder();
		
		OrderService orderService = Mockito.mock(OrderService.class);
		
		pizzeriaController.processOrder(orderService, jeanOrder.getCustomer(), jeanOrder.getWaiter(), jeanOrder.getProducts());
		
		Mockito.verify(orderService, Mockito.times(1)).orderAlaCarte(jeanOrder.getProducts().get(0), jeanOrder.getCustomer(), jeanOrder.getWaiter());
	}
	
	@Test
	void testTransmitOrderToRenderIsContentByAView() {
		
		PizzeriaController pizzeriaController = new PizzeriaController();
		
		View consoleView = Mockito.mock(ConsoleView.class);
		
		Order order = Mockito.mock(Order.class);
		
		pizzeriaController.render(consoleView, order);
		
		Mockito.verify(consoleView, Mockito.times(1)).display(order);
		
	}

}
