package it.pizzeria.domain.customer;

import java.util.Arrays;
import java.util.List;

import it.pizzeria.domain.catalog.Boisson;
import it.pizzeria.domain.catalog.Dessert;
import it.pizzeria.domain.catalog.Menu;
import it.pizzeria.domain.catalog.Plat;
import it.pizzeria.domain.catalog.Product;

public class Jean implements Customer {

	@Override
	public List<Product> chooseProductsFromMenu(Menu menu) {
		
		return Arrays.asList(new Plat(), new Boisson(), new Dessert());
	}

	@Override
	public String getNom() {		
		return "Jean";
	}

}
