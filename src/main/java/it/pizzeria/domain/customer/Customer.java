package it.pizzeria.domain.customer;

import java.util.List;

import it.pizzeria.domain.catalog.Menu;
import it.pizzeria.domain.catalog.Product;

public interface Customer {

	List<Product> chooseProductsFromMenu(Menu menu);

	String getNom();

}
