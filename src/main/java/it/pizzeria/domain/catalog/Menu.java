package it.pizzeria.domain.catalog;

public interface Menu {

	int countProducts();

	Product getProducts();

}
