package it.pizzeria.domain.catalog;

public interface Product {

	public default String getNom() {
        return this.getClass().getSimpleName();
    }

}
