package it.pizzeria.domain.waiter;

import it.pizzeria.domain.catalog.Menu;
import it.pizzeria.domain.customer.Customer;
import it.pizzeria.domain.customer.ReflectionTime;
import it.pizzeria.domain.furniture.Table;
import it.pizzeria.domain.order.JeanOrder;
import it.pizzeria.domain.order.Order;

public class Sofia implements Waiter {

	@Override
	public boolean installCustomerToTable(Customer customer, Table table) {
		
		if(table.isAvailable())
			return true;
		
		return false;
	}

	@Override
	public boolean bringMenuToClient(Menu menu, Customer customer) {
		
		return true;
	}

	@Override
	public boolean givesSomeReflectionTimeToCustomer(ReflectionTime moment, Customer customer) {
		
		return true;
	}

	@Override
	public Order takeOrderFromCustomer(Customer customer) {
		
		return new JeanOrder();
	}

	@Override
	public String getNom() {
		
		return "Sofia";
	}

}
