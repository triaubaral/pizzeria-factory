package it.pizzeria.domain.waiter;

import it.pizzeria.domain.catalog.Menu;
import it.pizzeria.domain.customer.Customer;
import it.pizzeria.domain.customer.ReflectionTime;
import it.pizzeria.domain.furniture.Table;
import it.pizzeria.domain.order.Order;

public interface Waiter {

	boolean installCustomerToTable(Customer customer, Table table);

	boolean bringMenuToClient(Menu menu, Customer customer);

	boolean givesSomeReflectionTimeToCustomer(ReflectionTime moment, Customer customer);

	Order takeOrderFromCustomer(Customer customer);

	String getNom();

}
