package it.pizzeria.domain.furniture;

public interface Table {

	boolean isAvailable();

}
