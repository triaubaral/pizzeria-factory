package it.pizzeria.domain.order;

import java.util.ArrayList;
import java.util.List;

import it.pizzeria.domain.catalog.Product;
import it.pizzeria.domain.customer.Customer;
import it.pizzeria.domain.customer.Jean;
import it.pizzeria.domain.waiter.Sofia;
import it.pizzeria.domain.waiter.Waiter;

public class OrderFactory implements Order {
	
	private List<Product> products = new ArrayList<Product>();
	
	public OrderFactory(Product product) {
		products.add(product);
	}
	
	public OrderFactory(List<Product> products) {
		this.products = products;
	}
		
	@Override
	public List<Product> getProducts() {
		return products;
	}

	@Override
	public Product getProduct() {
		return products.get(0);
	}

	@Override
	public Customer getCustomer() {
		
		return new Jean();
	}

	@Override
	public Waiter getWaiter() {
		
		return new Sofia();
	}

	

}
