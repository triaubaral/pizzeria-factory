package it.pizzeria.domain.order;

import java.util.List;

import it.pizzeria.domain.catalog.Product;
import it.pizzeria.domain.customer.Customer;
import it.pizzeria.domain.waiter.Waiter;

public interface Order {
	
	Product getProduct();

	List<Product> getProducts();

	Customer getCustomer();

	Waiter getWaiter();	

}
