package it.pizzeria.domain.order;

import java.util.ArrayList;
import java.util.List;

import it.pizzeria.domain.catalog.Pizza4Fromages;
import it.pizzeria.domain.catalog.Product;
import it.pizzeria.domain.customer.Customer;
import it.pizzeria.domain.customer.Jean;
import it.pizzeria.domain.waiter.Sofia;
import it.pizzeria.domain.waiter.Waiter;

public class JeanOrder implements Order {
	
	private Product product = new Pizza4Fromages();
	private Customer jean = new Jean();
	private Waiter sofia = new Sofia();
	
	@Override
	public Product getProduct() {
		return product;
	}

	@Override
	public List<Product> getProducts() {
		List<Product> data = new ArrayList<Product>();
		data.add(product);
		return data;
	}

	@Override
	public Customer getCustomer() {
		return jean;
	}

	@Override
	public Waiter getWaiter() {
		return sofia;
	}
	
	
	
	

}
