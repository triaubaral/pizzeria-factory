package it.pizzeria.service;

import it.pizzeria.domain.catalog.Menu;
import it.pizzeria.domain.catalog.Product;
import it.pizzeria.domain.customer.Customer;
import it.pizzeria.domain.order.Order;
import it.pizzeria.domain.order.OrderFactory;
import it.pizzeria.domain.waiter.Waiter;

public class OrderService {

	public Order orderAlaCarte(Product product, Customer customer, Waiter sofia) {	
		
		return new OrderFactory(product);		
		
	}
	
	public Order orderMenu(Menu menu, Customer customer, Waiter sofia) {		
		
		return new OrderFactory(menu.getProducts());		
		
	}
	
}
