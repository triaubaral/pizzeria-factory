package it.pizzeria.presentation;

import java.util.List;

import it.pizzeria.domain.catalog.Product;
import it.pizzeria.domain.customer.Customer;
import it.pizzeria.domain.order.Order;
import it.pizzeria.domain.waiter.Waiter;
import it.pizzeria.service.OrderService;

public class PizzeriaController {

	
	public void processOrder(OrderService orderService, Customer customer, Waiter waiter, List<Product> products) {
				
		orderService.orderAlaCarte(products.get(0), customer, waiter);
		
	}

	public void render(View view, Order order) {
		
		view.display(order);
	}

}
