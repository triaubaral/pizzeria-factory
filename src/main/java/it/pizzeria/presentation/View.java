package it.pizzeria.presentation;

import it.pizzeria.domain.order.Order;

public interface View {
	
	String display(Order order);

}
