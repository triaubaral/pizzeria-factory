package it.pizzeria;

import it.pizzeria.domain.order.JeanOrder;
import it.pizzeria.presentation.ConsoleView;

public class PizzeriaApp {

	public static void main(String[] args) {
		
		ConsoleView console = new ConsoleView();
		
		JeanOrder order = new JeanOrder();
		
		System.out.println(console.display(order));				

	}

}
